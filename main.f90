program main
  use, intrinsic :: iso_c_binding 
  use mpi
  use shm_mpi3
  implicit none

  include 'fftw3.f03'
  integer i, j, id, np, nl, na, nruns, ierr
  integer, allocatable :: map(:)
  complex(kind(0.d0)), allocatable :: a(:,:), fa(:,:), ra(:,:),  b(:,:), fb(:,:)
  complex(kind(0.d0)), pointer     :: s(:,:) => null(), r(:,:) => null()
  real(kind(0.d0)) ts,te

  type(c_ptr) planf, planb

  call mpi_init(ierr)
  call shm_init(mpi_comm_world)

  call init

  call mpi_barrier(mpi_comm_world, ierr)

  ! fft in x
  planf = fftw_plan_dft_1d(na, a(:,1), fa(:,1), FFTW_FORWARD, FFTW_ESTIMATE);
  planb = fftw_plan_dft_1d(na, a(:,1), fa(:,1), FFTW_BACKWARD, FFTW_ESTIMATE);

  ts = mpi_wtime()
  do i=1, nruns
     do j=1,nl
        call fftw_execute_dft(planf,a(:,j),fa(:,j))
     enddo
     call transpose(fa,b)
     do j=1,nl
        call fftw_execute_dft(planf,b(:,j),fb(:,j))
     enddo

     call transpose(fb,ra)

     do j=1,nl
        call fftw_execute_dft(planb,ra(:,j),fa(:,j))
     enddo
     
     call transpose(fa,b)

     do j=1,nl
        call fftw_execute_dft(planb,b(:,j),fb(:,j))
     enddo

    call transpose(fb,ra) 

  !call transpose_inv
  enddo
  te = mpi_wtime()

  call time_statistics(ts,te,nruns,'mpi')

  call test_ft(a,ra)

  call mpi_barrier(mpi_comm_world, ierr)
  
  ts = mpi_wtime()
  do i=1,nruns
     do j=1,nl
        call fftw_execute_dft(planf,a(:,j),s(:,j))
     enddo
     call shm_transpose(s,r)
     do j=1,nl
        call fftw_execute_dft(planf,r(:,j),s(:,j))
     enddo
      call shm_transpose(s,r)
      do j=1,nl
        call fftw_execute_dft(planb,r(:,j),s(:,j))
     enddo
     call shm_transpose(s,r)
     do j=1,nl
        call fftw_execute_dft(planb,r(:,j),s(:,j))
     enddo
     call shm_transpose(s,r)
  enddo
  !call shm_transpose_inv
  te = mpi_wtime()
  call time_statistics(ts,te,nruns,'shm')
   call test_ft(a,r)

  !call test_tr(s,r)
  
  if (i< 0) then 
     call hack(b,s)
  endif

  call fftw_destroy_plan(planf)
  call fftw_destroy_plan(planb)
  call shm_free(s)
  call shm_free(r)
  call shm_clean
  call mpi_finalize(ierr)

contains

  subroutine init
    implicit none

    integer i
    integer, allocatable :: buff(:)
    character(len=32) val
    
    ! get na from envoronment
  
    call mpi_comm_rank(mpi_comm_world, id, ierr)
    call mpi_comm_size(mpi_comm_world, np, ierr)
   
    call get_command_argument(1, value=val, length=i)
    if ( i==0) then 
       write(0,*) "no matrix size provided, quitting .."
       call mpi_abort(MPI_COMM_WORLD,-1,ierr)
    endif
    read(val,*) na

    call get_command_argument(2, value=val, length=i)
    if ( i==0) then 
       nruns=5
       if (id == 0) write(*,*) "nruns set to 5"
    else
       read(val,*) nruns
    endif


    if (id == 0) write(0,*) 'na ', na
    
    nl=na/np
    if (id < mod(na,np))  nl = nl + 1
    allocate(buff(0:np-1), map(-1:np-1),&
         a(na,nl), fa(na,nl), ra(na,nl), b(na,nl), fb(na,nl))
    
    call mpi_allgather(nl, 1, MPI_INTEGER, buff, 1, MPI_INTEGER, mpi_comm_world, ierr)
    map(-1)=0
    map(0)=buff(0)
    do i=1, np-1
       map(i)=map(i-1)+buff(i)
    enddo

    do i =1, nl
       a(:,i) = 1.0 !i+10*(id+1)
    enddo
    b=-1

    call shm_alloc(s,(/ 1, na, 1, nl /))
    call shm_alloc(r,(/ 1, na, 1, nl /))

    do i =1, nl
       s(:,i) = i+10*(id+1)
    enddo
    r=-1

    !write(0,*) id, map

  end subroutine init

  
  subroutine transpose(a,b)
    implicit none
    complex(kind(0.d0)), intent(in ) :: a(:,:)
    complex(kind(0.d0)), intent(out) :: b(:,:)

    integer i, ii, j1, j2, n, dest, src,ierr
    complex(kind(0.d0)) buff((na/np+1)*(na/np+1))

    do ii = 1, np-1
       dest = mod(id+ii,np)        
       src  = mod(id+np-ii,np)
       i=max(ii,np-ii)
       if (mod(id/i,2) == 0) then 
          n=(map(src)-map(src-1))*(map(id)-map(id-1))
          call mpi_recv(buff,n,MPI_DOUBLE_COMPLEX,src,ii,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr) 
          n=(map(dest)-map(dest-1))*(map(id)-map(id-1))
          call mpi_send(a(map(dest-1)+1:map(dest),:),n,MPI_DOUBLE_COMPLEX,&
               dest,ii,MPI_COMM_WORLD,ierr)
       else
          n=(map(dest)-map(dest-1))*(map(id)-map(id-1))
          call mpi_send(a(map(dest-1)+1:map(dest),:),n,MPI_DOUBLE_COMPLEX,&
               dest,ii,MPI_COMM_WORLD,ierr)
          n=(map(src)-map(src-1))*(map(id)-map(id-1))
          call mpi_recv(buff,n,MPI_DOUBLE_COMPLEX,src,ii,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
       end if
       ! transpose
       do j2 = 1, nl
          do j1 = map(src-1)+1, map(src)
             b(j1, j2) = buff(j2+(j1-map(src-1)-1)*nl)
          enddo
       enddo
    enddo

    ! the local transpose
    do j2 = 1, nl
       do j1 = map(id-1)+1, map(id)
          b(j1, j2) = a(map(id-1)+j2, j1-map(id-1)) !(j2+(j1-map(i-1)-1)*(map(i)-map(i-1)))
       enddo
    enddo
    
    
  end subroutine transpose


  subroutine shm_transpose(s,r)
    implicit none
    complex(kind(0.d0)), intent(in ) :: s(:,:)
    complex(kind(0.d0)), intent(out) :: r(:,:) 

    integer i, ii, j1, j2, ipn, n, dest, src,ierr
    complex(kind(0.d0)) buff((na/np+1)*(na/np+1))
    complex(kind(0.d0)), pointer :: nd_ptr(:,:) => null()
    
    call shm_node_barrier
    
    do ipn = 0,shm_info%size - 1
       src = shm_info%wranks(ipn)
       if (src == id) then
          ! the local traspose
          do j2 = 1, nl
             do j1 = map(id-1)+1, map(id)
                r(j1, j2) = s(map(id-1)+j2, j1-map(id-1)) 
             enddo
          enddo
       else
          nd_ptr => shm_get_node_pointer(s, ipn)
          do j2 = 1, nl
             do j1 = map(src-1) +1,  map(src)
                r(j1, j2) = nd_ptr(map(id-1)+j2, j1-map(src-1)) 
             enddo
          enddo
       endif
    enddo

    do ii = 1, np-1
       dest = mod(id+ii,np)        
       src  = mod(id+np-ii,np)
       i=max(ii,np-ii)
       if (mod(id/i,2) == 0) then 
          if (.not. shm_onnode(src)) then
             n=(map(src)-map(src-1))*(map(id)-map(id-1))
             call mpi_recv(buff,n,MPI_DOUBLE_COMPLEX,src,ii,&
                  MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
          endif
          if (.not. shm_onnode(dest)) then
             n=(map(dest)-map(dest-1))*(map(id)-map(id-1))
             call mpi_send(s(map(dest-1)+1:map(dest),:),n,MPI_DOUBLE_COMPLEX,&
                  dest,ii,MPI_COMM_WORLD,ierr)
          endif
       else
          if (.not. shm_onnode(dest))then
             n=(map(dest)-map(dest-1))*(map(id)-map(id-1))
             call mpi_send(s(map(dest-1)+1:map(dest),:),n,MPI_DOUBLE_COMPLEX,&
               dest,ii,MPI_COMM_WORLD,ierr)
          endif
          if (.not. shm_onnode(src))then
             n=(map(src)-map(src-1))*(map(id)-map(id-1))
             call mpi_recv(buff,n,MPI_DOUBLE_COMPLEX,src,ii,&
                  MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
          end if
       endif
       ! transpose
       if ( .not. shm_onnode(src))then
          do j2 = 1, nl
             do j1 = map(src-1)+1, map(src)
                r(j1, j2) = buff(j2+(j1-map(src-1)-1)*nl)
             enddo
          enddo
       endif
    enddo

    call shm_node_barrier
  end subroutine shm_transpose



  subroutine test_tr(a,b)
    implicit none
    
    real(kind(0.d0)), intent(in) :: a(:,:), b(:,:)
    integer i,ierr
    character(len=256) fstr

    real(kind(0.d0)) at(na,na)

    call mpi_gatherv(a,na*nl,MPI_DOUBLE_PRECISION,&
         at, na*(map(0:np-1) -map(-1:np-2)), na*map(-1:np-2),&
         MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)
    
    if (id == 0) then
       write(*,'(1x/1x/)')
       do i=1, na
          write(*,'(200f3.0,1x)') real(at(i,:))
       enddo
    endif
    
    call mpi_gatherv(b,na*nl,MPI_DOUBLE_PRECISION,&
         at, na*(map(0:np-1) -map(-1:np-2)), na*map(-1:np-2),&
         MPI_DOUBLE_PRECISION, 0, mpi_comm_world, ierr)
       
    if (id == 0) then
       write(*,'(1x/1x/)')
       do i=1, na
          write(*,'(200f3.0,1x)') real(at(i,:))
       enddo
    endif

  end subroutine test_tr


  subroutine test_ft(a,b)
    implicit none
    
    complex(kind(0.d0)), intent(in) :: a(:,:), b(:,:)
    integer i,ierr
    character(len=256) fstr

    real(kind(0.d0)) x, norm

    x = sum(abs(a-b/na**2))

    call mpi_reduce(x,norm,1,MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
      
      if (id==0) then 
         write(*,*)"# FFT test ", norm
      endif

    end subroutine test_ft


  subroutine time_statistics(ts, te, nruns, msg)
    implicit none
    
    real(kind(0.d0)), intent(in) :: ts, te
    integer, intent(in) :: nruns
    character(len=*), optional, intent(in) :: msg
    integer ierr
    real(kind(0.d0)) t, tsum, tmin, tmax
    
    t = te - ts
    call mpi_reduce(t,tmin,1,MPI_DOUBLE_PRECISION, MPI_MIN, 0, MPI_COMM_WORLD, ierr)
    call mpi_reduce(t,tmax,1,MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD, ierr)
    call mpi_reduce(t,tsum,1,MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
      
      if (id==0) then 
         if (present(msg))then 
            write(*,'(a)')'# '//msg
         endif
         write(*,'(a,/,i6,1x,3(E10.3,1x))')"# Transpose times per run: nproc, average, min, max ",&
              np, tsum/(nruns*real(np)), tmin/nruns, tmax/nruns
      endif

    end subroutine time_statistics

  subroutine hack(a,b)
    implicit none
    ! tries to avoid superoptimisation
    ! triks the compiler to believe that data migh go somewhere
    complex(kind(0.d0)) a(:,:), b(:,:)
    
    write(*,*) a(1,1), b(1,1)
  end subroutine hack

end program main
