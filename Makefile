FC = mpif90

ifdef DBG
  FFLAGS := -fcheck=all -g
else
  FFLAGS := -O2
endif

FFT_LIB := -L/usr/local/lib -lfftw3
FFT_INC := -I/usr/local/include

EXE=t.exe

all: $(EXE)
INC_FILES := $(wildcard *.inc)

$(EXE): main.o  shm_mpi3.o
	$(FC) $(FFLAGS) -o $(EXE) $^ $(FFT_LIB)

clean:
	rm -f *.o

%.o : %.f90
	$(FC) -c -o $@ $(FFT_INC) $(FFLAGS) $<

main.o : main.f90 shm_mpi3.o
shm_mpi3.o : shm_mpi3.f90 $(INC_FILES)




